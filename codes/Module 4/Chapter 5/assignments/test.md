# Software Engineering Virtual Lab
## Introduction
![Image](image.png)
> Research analysis manifests clear trends of growing interest of the Computer Science students towards e-learning. To keep this trend as well as the pace with the rapid advancement of software technologies, the "Software Engineering Virtual Lab" has been developed. This lab attempts to provide basic concepts to the students on a wide range of topics including Requirements analysis, ER modeling, identification of domain classes, use-case diagrams and designing a test suite. Ample illustrations and simulations are used to reinforce one's understanding. Once the concepts are clear, a set of exercises given on the concerned topics would help the students to evaluate themselves and their progress of learning. The main focus is to enable the students to interact with the "virtual" teacher in an effective and efficient manner compared to how they would do in a real lab on the subject.
## Objectives

> The Software Engineering Virtual Lab has been developed by       keeping in mind the following objectives:
  + To impart state-of-the-art knowledge on Software Engineering and UML in an interactive manner through the Web
  + Present case studies to demonstrate the practical applications of different concepts
  + Provide a scope to the students where they can solve small, real life problems
> All the while it is intended to present Software Engineering as an interesting subject to the students where learning and fun can go alongside.
## List of Experiments
1. [Identifying the Requirements from Problem Statements](http://vlabs.iitkgp.ernet.in/se/1/)
2. [Estimation of Project Metrics](http://vlabs.iitkgp.ernet.in/se/2/)

## Target Audience
> This lab is suitable for undergraduate or postgraduate students having a course on Software Engineering. An overview of Object-Oriented Programming would be helpful, although not manadatory. A little familiarity with JavaScript is required to work on the experiment on "Designing test suite"
## System Requirements
> You need a modern web browser with JavaScript enabled to access the complete set of features of Software Engineering Virtual Lab. In particular, this lab has been tested and found to work successfully with

+ ![Image](firefox.png "Firefox 3.6, 5.0")  Firefox 3.6, 5.0
+ ![Image](chrome.png "Google Chrome 13.0") Google Chrome 13.0
+ ![Image](internet_explorer.png "Internet Explorer 7") Internet Explorer 7
+ ![Image](opera.png "Opera 11.0") Opera 11.0
> Any higher version of the above mentioned four web browsers would also work fine. Moreover, any web browser using modern versions of Gecko, WebKit rendering engines should be able to display properly and access all the features.