function multiply() {
    var count = document.getElementById("n");
    var table = document.getElementById("table");
    var n = +(count.value); //to typecast into integer value
    var i = 1;
    while (table.rows.length > 1) //while loop to delete the row
     {
        table.deleteRow(1);
    }
    console.log(n);
    for (i = 1; i <= n; i++) {
        var row = table.insertRow(); //insert a row
        var cell1 = row.insertCell(); //insert cell in the row
        var cell2 = row.insertCell();
        var cell3 = row.insertCell();
        cell1.innerHTML = n.toString() + " * " + i.toString(); //assign value to cell
        cell2.innerHTML = " = ";
        cell3.innerHTML = (n * i).toString();
    }
}
//# sourceMappingURL=dynamic.js.map